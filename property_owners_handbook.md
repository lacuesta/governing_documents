# Property Owners Handbook 
**Mailing Address:**  

> PMB 114  
> 8390 East Via de Ventura, F-110  
> Scottsdale, AZ 85258  


## Welcome to La Cuesta Association!

We are an established Association that is responsible for the maintenance of common areas within our community, as well as to administer and enforce use restrictions and set property standards and procedures as contained in the Articles of Incorporation; the Bylaws; the Covenants, Conditions and Restrictions (CC&R's) and adopted rules.

This Handbook has been prepared as a brief summary and reference for some of that information. We trust it will be of assistance. As residents we need to be informed, or perhaps reminded, of our community standards. For complete details, please refer to the original documents.

To provide funds for maintenance and other functions of the Association, all properties within La Cuesta are subject to an annual assessment payable to the La Cuesta Association. Membership in the La Cuesta Association includes every person or entity that is an owner of record and is subject to the Covenants of Record.

We wish to express our gratitude for your help in doing your part to keep La Cuesta looking its best and to maintain an ambiance of community pride.

_**La Cuesta Board of Directors**_


## Contents

[Meetings](https://gitlab.com/lacuesta/governing_documents/-/blob/main/property_owners_handbook.md?ref_type=heads#meetings)  
[Architectural Committee](https://gitlab.com/lacuesta/governing_documents/-/blob/main/property_owners_handbook.md?ref_type=heads#architectural-committee-ac)  
[Assessments](https://gitlab.com/lacuesta/governing_documents/-/blob/main/property_owners_handbook.md?ref_type=heads#assessments)  
[Collection Policy](https://gitlab.com/lacuesta/governing_documents#assessments)  
[Common Area](https://gitlab.com/lacuesta/governing_documents#collection-policy-for-la-cuesta-association)  
[Resident Obligations](https://gitlab.com/lacuesta/governing_documents/-/blob/main/property_owners_handbook.md?ref_type=heads#residents-obligations)  
[Tennis Courts Rules](https://gitlab.com/lacuesta/governing_documents/-/blob/main/property_owners_handbook.md?ref_type=heads#tennis-court-rules)  
[Rules](https://gitlab.com/lacuesta/governing_documents/-/blob/main/property_owners_handbook.md?ref_type=heads#la-cuesta-rules)  
[Fine Policy](https://gitlab.com/lacuesta/governing_documents/-/blob/main/property_owners_handbook.md?ref_type=heads#la-cuesta-fine-policy)  


## MEETINGS
Your Board of Directors currently is convening to discuss your Association business on a monthly basis on the fourth Tuesday of each month at 7:00 p.m. at Scottsdale Bible Church on the corner of Miller and Shea. Meetings are open to all interested homeowners. Before each Board meeting there is an open forum for homeowners to speak with the Board about issues of concern or interest. **We encourage and welcome your participation in all Board and community activities.**

The Annual Meeting of the members will be held each year on the fourth Tuesday of March at 7:30 p.m. The location will be announced, by mail, at least 30 days prior to the meeting. The election of Board members is held at this meeting.

The affairs of the Association are conducted by a Board of Directors and such Officers as may be elected. Your Board may also adopt and/or repeal rules and regulations to be known as Rules as long as they are consistent with the provisions of the CC&R's. These Rules shall have the same force and effect as if they were set forth in and were part of the original CC&R's.

## ARCHITECTURAL COMMITTEE (AC)
Article VII of the Declaration of Covenants, Conditions and Restrictions (CC&R's) provides for an Architectural Committee (AC), which has the duty to set community standards for landscaping, color schemes and exterior maintenance. **Any changes, additions or alterations to the exterior of your property must be approved by the Architectural Committee**. To expedite an approval, obtain the architectural change form from the committee or a Board member and submit it with an appropriate sketch or drawing illustrating the changes, dimensions, paint color swatches, and types of materials to be used. Members of the Architectural Committee are available to answer questions in this regard. ***Color changes are subject to approval by the Architectural Committee.***

**DO NOT UNDERTAKE ANY ADDITION OR CHANGE TO THE EXTERIOR OF YOUR HOME WITHOUT CONSULTING THE ARCHITECTURAL COMMITTEE.**

*For further details and explanation, please refer to Article VII, Use Restrictions of the Declaration of Covenants, Conditions and Restrictions. (CC&R's).*

Those homes that today are not in compliance with Architectural Committee rules shall bring them into compliance by the next maintenance opportunity or prior to the close of escrow, whichever occurs first.


## ASSESSMENTS

Assessments, as established in the CC&R's and by the Board of Directors, are necessary to provide funds for landscape improvements, continuing maintenance and administration of the Association. If payments are not made on a timely basis, the Association has the right to assess late fees and to pursue legal remedies, which may include interest charges, attorney's fees and placement of a lien on your property.

***Property owners whose assessments are in arrears are not considered members in good standing and therefore do not have voting privileges or use of La Cuesta common areas.***

### Collection Policy for La Cuesta Association

1. Semi-annual assessments are invoiced on December 1 and June 1 and are mailed to the last known address of the member. Members have the option to pay for one year.

2. Any members not remitting their assessments within 35 days of the invoice date shall be sent an invoice for a late charge of \$15.00 or 10% of the unpaid balance, whichever is greater. If payment of all assessments and late charges is not received within 15 days, legal action may commence.

3. La Cuesta will notify its legal firm about the delinquencies and lack of member response. The firm will send letters to the delinquent members stating that, if payment is not received within 30 days, legal action to collect all assessments and late charges owed to La Cuesta plus all legal fees incurred by La Cuesta in connection with this action will commence.

4. If there is still no acceptable action by the member to resolve this issue, La Cuesta Association will, at its option, instruct the law firm, at member cost, to file suit for a monetary judgment or file a lien foreclosure in Superior Court, or other action as allowed by Arizona Law from time to time.

**Board Revised & Approved** February 28, 2006


## COMMON AREA

The major expense of the Association is maintenance of the common areas. We have two tennis courts located on Cannon Drive, several islands of landscaping within the property, as well as perimeter landscape outside your property walls along Shea Blvd., Mt View Rd., 78th St, and Gold Dust Avenue. We provide regular maintenance of the landscaping, periodic painting of the common walls, periodic resurfacing and regular cleaning of the tennis courts and occasional improvements in the landscaping.


## RESIDENT'S OBLIGATIONS

La Cuesta is your community. By accepting a deed to property within La Cuesta you agreed to abide by the Covenants, Conditions and Restrictions recorded on the property before you purchased it. Knowingly or unknowingly some residents violate the protective restrictions. In most cases, through our informal notice procedure, the situation is corrected. While the restrictions are sometimes seemingly complex, they can be boiled down to a few simple rules that, if followed, will ensure that the objectives of La Cuesta will be met and there will be no need for complaints from your neighbors or your Association.


## TENNIS COURT RULES

The following rules pertain to conduct on the La Cuesta courts for the enjoyment of all.

1. Tennis courts are for the exclusive use of residents and accompanied guests.
2. Tennis court instruction of non-residents for compensation is prohibited.
3. Tennis courts are for tennis use only.
4. Tennis shoes must be worn on the courts.
5. Open hours are from 6:00 a.m., or sunrise to 10:00 p.m.
6. Replacement keys are available for a fee from the Secretary.
7. Play is limited to one hour when others are waiting to play.

## La Cuesta Rules

The following are our rules. For further details please refer to the original Association documents.

1. Each property shall be used exclusively as a single-family dwelling. Garages shall be used for parking vehicles and storage purposes only and shall not be converted for living or recreational activities without the written consent of the AC.

2. No improvement, alteration, repairs, excavation or other work is allowed without AC approval. No building, fence, basketball standard, wall or other structure may be erected without prior AC approval. Solar Panels and solar energy devices are subject to prior architectural approval. They will need to be oriented so they are not visible from the front of any property.

3. Your property must be maintained in good condition and repair and adequately painted or otherwise finished.

4. All trash shall be placed in containers and **kept out of sight** except for trash pickup days. No rubbish or debris of any kind shall be allowed to accumulate on any property. Once a month the city has a brush pick-up program. _All materials must be placed at the curb in front of your home no earlier than the Saturday of the week preceding the pick-up._ Check the notices with your water bill for the exact week of pick-up.

5. Landscaping: Property must be kept free of weeds. Hedges, trees and shrubs are to be trimmed 12 ft. over the sidewalks and away from sidewalks and otherwise maintained. The Association recommends the removal of seedpods and dead fronds from palm trees in June of each year to reduce pest infestation.

6. Pets: PLEASE TAKE PRIDE IN YOUR NEIGHBORHOOD! Respect La Cuesta common areas and the personal property of other homeowners by **picking up after your dog**. A small baggie turned inside out provides a good disposable container. Homeowners are reminded that Scottsdale has a city code that includes a leash law for dogs and prohibits excessive barking.

7. Except as provided, only allowable vehicles in good operating condition and in good visual repair of body and engine shall be parked in uncovered parking areas.  

Definition of Allowable Vehicles:

a. Passenger Cars: "Cars manufactured on a standard car chassis."  
b. Vans, Sport Utility Vehicles, Station Wagons, Pickup Trucks: "Vehicle does not exceed 7 feet in height and 19 feet in length."  

This allowable vehicle definition applies equally to a vehicle whether it has a standard car license plate, a Non-Commercial License Plate, or a Commercial License Plate (i.e., if registered to a business).  

Definition of Restricted Vehicles;  
Mobile Home/Motor Home, Boat, Trailer of any kind, Commercial Vehicles, Trucks exceeding 7 feet in height or 19 feet in length or displaying any commercial name, phone number or messages.

8. The Board, at its sole discretion, shall have the right to determine the existence of any noise nuisance. Please respect your neighbor's quiet enjoyment of his/her property.

9. Outside clotheslines shall not be visible from neighboring property.

10. A fourth concrete driveway lane is not allowable.

11. Swimming pool excavation is allowed if done by a certified contractor providing there is prior AC approval.

12. For Sale and For Lease signs may be placed only on your property.

13. Residents are encouraged to park vehicles off the streets to maintain the quality of appearance of La Cuesta.

14. Antennas and/or satellite dishes that are covered under FCC are subject to the following rules:

a. Antennas shall be installed solely on the Owner's lot, not Common Area.
b. Antennas shall be located in a place shielded form view to the maximum extent possible while still receiving an acceptable quality signal.
c. Antennas shall neither be large nor installed higher than is necessary for receiving an acceptable quality signal.
d. Residents shall not permit Antennas to fall into disrepair or become a safety hazard.
e. Antennas shall be neutral in color or painted to match the color of the structure on which they are installed.
f. If an Antenna is on the ground and would be visible, it should be camouflaged by landscaping or screening, and all wiring shall be installed to be minimally visible.


## Architectural Committee Rule
From Article VIII, Section 1.D., the Architectural Committee is charged with considering the "harmony thereof with the surroundings and the effect of the building or other structure as planned, on the outlook from the adjacent or neighboring Property". For typical playground equipment visible from the street or neighboring property, the following set of simple guidelines will be considered when reviewing requests for approval to help us to preserve that harmony within La Cuesta:

### Play Equipment
10' height maximum  
Canopy color - neutral - (no primary colors)  
(Also see; Declaration of CC&R's, Article VII, Section 4.)

**Board Revised & Approved** February 28, 2006

Your assistance and cooperation in adhering to the Recorded Use Restrictions and Rules is deeply appreciated.

## La Cuesta Fine Policy

A member of La Cuesta Association who is found to be in violation of the Declaration of Covenants, Conditions and Restrictions, Bylaws, Rules, or any other provision of the governing documents of La Cuesta Association will be notified in writing, sent by mail or hand delivered, of the nature of the violation. The notice of violation shall provide that within a period of time after receipt of the notice of violation, the member must either correct the violation or enter into a compliance agreement acceptable to the Board whereby the member agrees to rectify the violation.

The La Cuesta Board of Directors shall have the power to impose monetary penalties upon the owners of Lots for the above violations. This power shall apply to violations by the owner(s) and the owner(s) shall also be liable for any violation committed by a family member, guest, tenant or other occupant of the Lot of the owners(s). The amount of the monetary penalties shall be determined based on the nature of the offense and the number of violations, and the amount so established by the Board of Directors shall range from \$10.00 to a maximum of \$500.00 per day. The owner(s) in question shall be given an opportunity to be heard by the Board prior to the assessing of any monetary penalties, and written notice of said hearing shall be given at least 10 days in advance of the hearing by regular mail or by hand delivery at the last-known address of the owner(s). Once it has been determined that the owner(s) is guilty of a continuing violation, the Board may impose reasonable daily monetary penalties for each subsequent occurrence of the violation and such continuing penalties shall continue to accrue until the owners(s) notifies the Board that the violation has ceased and the Board has confirmed that, this, is in fact the case. If any penalties assessed against the owner(s) remain unpaid, the Association can obtain a monetary judgment against owner(s) for such amounts.

**Board Revised & Approved** February 28, 2006
