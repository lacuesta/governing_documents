# ARTICLES OF INCORPORATION Of LA CUESTA ASSOCIATION

The undersigned, as incorporators, have this date, voluntarily associated themselves together for the purpose of forming a private non-profit membership corporation under and by virtue of the laws of the State of Arizona and do hereby adopt the following Articles of Incorporation:

## ARTICLE I
### NAME

The name of this corporation shall be LA CUESTA ASSOCIATION.

## ARTICLE II
### PRINCIPAL OFFICE

The principal place of business and office for the transaction of business of this corporation shall be located in or near the City of Scottsdale, Maricopa County, State of Arizona.

## ARTICLE III
### PURPOSE AND POWERS OF THE ASSOCIATION

This corporation shall be a non-profit corporation without pecuniary gain or profit to the members thereof, and the specific purposes for which it is formed are to provide for maintenance, preservation and architectural control of the residence lots and common areas within that certain tract of property described as:

> Lots 1 through 48, inclusive, and Drainage and Flood Control Easement of LA CUESTA as it appears in the books and records of the County of Maricopa, Arizona, Book 194 of Maps, Page 34;

and to promote the health, safety and welfare of the residents within the above-described property and any additions thereto as may hereafter, be brought within the jurisdiction of this Association; and, for these purposes, to:

(a) Exercise all of the powers and privileges and to perform all of the duties and obligations of the Association, as set forth in that certain Declaration of Covenants, Conditions and Restrictions, hereinafter called "Declaration", applicable to the property and recorded or to be recorded in the Office of the County Recorder of Maricopa County, Arizona, and as the same may be amended from time to time as therein provided, said Declaration being incorporated herein as if set forth at length;

(b) Fix, levy, collect and enforce payment by any lawful means all charges or assessments pursuant to the terms of said Declaration; to pay all expenses in connection therewith and all office and other expenses incident to the conduct of business of the Association, including all licenses, taxes or governmental charges levied or imposed against the property of the Association;

(c) Acquire (by gift, purchase or otherwise), own, hold, improve, build upon, operate, maintain, convey, sell, lease, transfer, dedicate for public use or otherwise dispose of real or personal property in connection with the Association;

(d) Borrow money and, with the assent of two-thirds (2/3) of each class of members, mortgage, pledge, deed in trust or hypothecate any or all of its real or personal property as security for money borrowed or debts incurred;

(e) Dedicate, sell or transfer all or any part of the common area to any public agency, authority or utility for such purposes and subject to such conditions as may be agreed to by the members. No such dedication or transfer shall be effective unless an instrument has been signed by two-thirds (2/3) of each class of members, agreeing to such dedication, sale or transfer;

(f) Participate in mergers and consolidations with other non-profit corporations organized for the same purposes or annex additional residential property and common area, provided that any such merger, consolidation or annexation shall be at the sole direction of the Declarant until March 15, 1983, after which date, such action shall have the assent of two-thirds (2/3) of each class of members;

(g) Additional residential property and common area within the area shown on the general plan of La Cuesta, as approved by Maricopa County, Arizona, may be annexed by Declarant without the consent of its members within five (5) years of the date of this instrument.

(h) Perform any acts or functions which may be legally carried out by a non-profit corporation organized under the laws of the State of Arizona as they presently exist and as they may be amended.

## ARTICLE IV
### MEMBERSHIP

Every person or entity who is a record owner of a fee or undivided fee interest in any lot which is subject to covenants of record to assessment by the Association, including contract sellers, shall be a member of the Association. The foregoing is not intended to include persons or entities who hold an interest merely as security for the performance of an obligation. Membership shall be appurtenant to and may not be separated from ownership of any lot which is subject to assessment by the Association.

## ARTICLE V
### VOTING RIGHTS

The Association shall have two (2) classes of voting membership:

Class A. Class A members shall be all owners with the exception of the Declarant/Developer and shall be entitled to one (1) vote for each lot owned. When more than one (1) person holds an interest in any lot, all such persons shall be members. The vote for such lot shall be exercised as they among themselves determine; but, in no event, shall more than one (1) vote be cast with respect to any lot.

Class B. The Class B member(s) shall be the Declarant/Developer (as defined in the Declaration) and shall be entitled to three (3) votes for each lot owned. The Class B membership shall cease and be converted to Class A membership on the happening of either of the following events, whichever occurs earlier:

> (a) When the total votes outstanding in the Class A membership equal the total votes outstanding in the Class B membership; or

> (b) December 31, 1985.

## ARTICLE VI
### BOARD OF DIRECTORS

The business and affairs of this corporation shall be managed by a Board of not less than three (3) nor more than twenty-five (25) Directors, who need not be members of the Association. The number of Directors may be changed by amendment of the Bylaws of the Association. The following persons were elected on March 1, 1978, at Scottsdale, Arizona, to serve as Directors until the election of their successors:

Joseph Contadino
4550 N. Black Canyon Highway
Phoenix, Arizona 85017

John W. Magura, Jr.
4550 N. Black Canyon Highway
Phoenix, Arizona 85017

Larry C. Fischer
4550 N. Black Canyon Highway
Phoenix, Arizona 85017

Joseph A. Brichler, Jr.
4550 N. Black Canyon Highway
Phoenix, Arizona 85017

Thereafter, the Board shall be elected at the regular annual meeting of the members which shall be held at Scottsdale, Maricopa County, Arizona, on the fourth Tuesday in March.

At the first annual meeting, the members shall elect three (3) Directors for a term of one (1) year, three (3) Directors for a term of two (2) years and three (3) Directors for a term of three (3) years; and, at each annual meeting thereafter, the members shall elect three (3) Directors for a term of three (3) years.

## ARTICLE VII
### LIABILITIES

Any indebtedness or liability, direct or contingent, must be authorized by an affirmative vote of a majority of the votes cast by the members of the Board of Directors at a lawfully held meeting. The highest amount of indebtedness or liability, direct or contingent, to which this corporation may be subject at any one time shall not exceed ONE HUNDRED FIFTY PERCENT (150%) of its income for the previous fiscal year, except that additional amounts may be authorized by an affirmative vote of two-thirds (2/3), of the membership. The private property of each and every officer, Director or member of this corporation shall, at all times, be exempt from all debts and liabilities of the corporation.


## ARTICLE VIII
## DISSOLUTION

The Association may be dissolved with the assent given in writing and signed by not less than two-thirds (2/3) of each class of members. Upon dissolution of the Association, other than incident to a merger or consolidation, the assets of the Association shall be dedicated to an appropriate public agency to be used for purposes similar to those for which this Association was created. In the event that such dedication is not accepted by an appropriate public agency, such assets shall be granted, conveyed and assigned to any non-profit corporation, association, trust or other organization to be devoted to similar purposes as described herein.


## ARTICLE IX
### DURATION

The time of commencement of this corporation shall be the date of the filing of the Articles of Incorporation by the Arizona Corporation Commission; and the termination thereof shall be twenty-five (25) years thereafter, with the privilege of renewal as provided by law, provided, however, that if the right of perpetual existence without the necessity of renewal shall at any time hereafter be granted by Arizona law, then the existence of the corporation shall be perpetual without necessity of renewal.

## ARTICLE X
### AMENDMENTS

These Articles may be amended during the first twenty (20) year period by an instrument signed by not less than seventy-five percent (75%) of the lot owners, and thereafter by an instrument signed by not less than fifty-one per- cent (51%) of the lot owners. Any amendment must be recorded.

## ARTICLE XI

The property which is described in Article III of these Articles is subject to the Declarations of Covenants, Conditions and Restrictions referred to in said Article III; and each purchaser of a lot in the La Cuesta subdivision takes his deed subject to all of the rights and obligations, including but not limited to the levy of assessments, contained in said Declaration of Covenants, Conditions and Restrictions.

## ARTICLE XII
### STATUTORY AGENT

CT Corporation Systems of Phoenix, Arizona, which is duly authorized by law to act as a Statutory Agent in the State of Arizona, is hereby appointed and made the agent to this corporation on whom all notices and processes, including service of summons, may be served and when so served shall be lawful personal service on this corporation. This appointment may be revoked at any time by the filing of the appointment of another agent.

## ARTICLE XIII
### FHA/VA APPROVAL

As long as there is a Class B membership, the following actions will require the prior approval of the Federal Housing Administration or the Veterans Administration: Annexation of additional properties, mergers and consolidations, mortgaging of common area, dedication of common area, dis- solution and amendment of these Articles.

IN WITNESS WHEREDF, for the purpose of forming this corporation under the laws of the State of Arizona, we, the undersigned, constituting the incorporators of this Associa- tion, have executed these Articles of Incorporation this 9th day of March, 1978.

 
STATE OF ARIZONA  )
                  ) ss:
County of Maricopa)

The foregoing Articles of Incorporation for LA CUESTA ASSOCIATION were acknowledged before me this this 9th day of March, 1978, by Joseph Contadino and John W. Magura, Jr.

 
Notary Public

My commission expires:
9-26-80
